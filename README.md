# Xiaomi Mi9t Davinci

KevP75's set of tweaks and services to keep your device running buttery smooth and keep that battery pumpin.

## What's it work on.

Should work fine on both MIUI and AOSP based roms.  I need testers for Lineage based.

## Requirements

- AOSP or Miui Based Rom
- Magisk 19+
- ADB Developer Options
- TWRP
- - Official and UnOfficial versions should work.  The installer only depends on `sh` being available

## Issues

Other than the above, I have found 0 issues.  During my testing I received 4 full days of battery life, with 7.5hrs SOT, and no performance issues.  Your mileage will probably vary as I am sure you use your device differently than I do.

## What's it do?

- During the install process it takes a backup of your /system/build.prop and your /system/etc/init/init.qcom.rc file, and then modifies the orginals by injecting some memory management, battery management, and performance properties.
- - backups are located in /data/local/kevp75/BACKUP
- Tweaks some schedutil parameters lowering the min frequency, and ramp up periods for high speed loads
- Disables some unnecessary logging
- Few file system tweaks for better UI responsiveness
- Tweaks the low memory killer a bit
- Trims a few of your mounts once per week, and on boot
- Forces the disabling of the wake locks property on some packages
- Forces the disabling of the run-in-background property on some packages
- - Some packages reset these settings when you re-open the apps.  These "blockers" run every hour and on boot.
- - There is a set of "log" files located at: /data/local/kevp75/logs
- - You can add packages to exclude for these.  
- - - /data/local/kevp75/data/rib_allowed is used for the "Run In Background" blocker.  Add your packages to exclude, on new lines.
- - - /data/local/kevp75/data/wl_allowed is used for the "Wake Locks" blocker.  Add your packages to exclude, on new lines.
- Does a bit of cleaning
- Injects true /system/etc/init.d support
- Change IO scheduler, and tweak
- Remove swap altogether
- - we have 6G of RAM in these, if you are using that much RAM, you should consider closing some apps more frequently.
- Caching managment tweaks


## How do I install it?
- Download the zip file: Kevp75-Tweaks.v.#####.zip
- Copy the zip file to your favorite location, then reboot to TWRP.
- Tap install
- Browse to the zip file and install it
- Wait until it finishes
- Reboot
- Enjoy

## How do I uninstall it?
- Download the zip file: Kevp75-Tweaks.Uninstaller.zip
- Copy the zip file to your favorite location, then reboot to TWRP.
- Tap install
- Browse to the zip file and install it
- Wait until it finishes
- Reboot
- Enjoy

## Coming Soon
- 