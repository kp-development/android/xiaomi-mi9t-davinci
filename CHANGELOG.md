# CHANGELOG

### v.2.8.1

    Reworked where, when, and how the kernel tweaks fire off
        All now work

### v.2.6.1

    Add journal removal on a few mounts during install
    Add battery calibration every 7 days
    Move user configurable whitelists
        Now located at: /data/local/kevp75/USER/

### v.2.5.4

    Add another package to WL allowed
    Modify blocker and trim runtimes
        now they'll run when intended.
            RIB and WL = on boot and every 1 hour
            FSTRIM = on boot and every 7 days
    Realized there's no "real" way for users to modify the whitelists
        added check for /mnt/sdcard/rib_allowed and /mnt/sdcard/wl_allowed
    

### v.2.5.3

    Add some more packages to both RIB and WL
    Add rotater for the logs

### v.2.5.2

    Move all actions from executions to services
        fixes the multiple times the tweaks/scripts will run
    
### v.2.5.1

    properly remove the .running files
    rework tweak placement in boot order
    enable IO tweaks
        set to deadline / 1024
    on boot: 
        force performance governor
        disable powersaving
        turn off energy awareness
        tweak schedtune
    on post boot complete:
        force schedutil
            and tweak soem settings
        enable powersaving
        reset schedtune
        enable energy aware
        Disable printk log
        Disable sched_stats
        low memory killer tweaks
        disable useless kernel debugging
        couple FS tweaks
        force deep sleep
        tweak caching size

**NOTE** Some of these will not work on MIUI based Roms

### v.2.4.2

    Fixed a typo in RIB blocker not reading the whitelist


### v.2.4.1

    Fixed fling tweaks - scrolling issues correcetd


### v.2.3.1

    Removed some tweaks for performance
    Integrated MIUI version


### v.2.2.3

    Change IO scheduler, and tweak
    Remove swap altogether
        we have 6G of RAM in these, if you are using that much RAM, you should consider closing some apps more frequently.
    Caching managment tweaks


### v.2.1.1

    Initial Release
