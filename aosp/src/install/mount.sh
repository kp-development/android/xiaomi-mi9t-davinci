#!/sbin/sh

# get our starting path
START=/dev/block/bootdevice/by-name/{vendor,system,cache,cust,dsp}

# unmount everything
umount -a;

# loop over the paths found
for D in $START*; 
do 

    # first we need to do a file system check
    e2fsck -y $D;

    # remove the journal flag
    tune2fs -O ^has_journal $D; 
    
done
