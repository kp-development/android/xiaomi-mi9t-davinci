#!/sbin/sh

# build prop
BUILDPROP_FILE=/system/build.prop;

# init file
INIT_FILE=/system/vendor/etc/init/hw/init.qcom.rc

# backup location
BACKUP_LOCATION=/data/local/kevp75/BACKUP;

# restore the original backup file if it exists
if [ -f "$BACKUP_LOCATION/build.prop" ]; then
    mv $BACKUP_LOCATION/build.prop $BUILDPROP_FILE;
fi;

# restore the original backup file if it exists
if [ -f "$BACKUP_LOCATION/init.qcom.rc" ]; then
    mv $BACKUP_LOCATION/init.qcom.rc $INIT_FILE;
fi;

# restore the original fstab
mv $BACKUP_LOCATION/fstab.qcom /system/vendor/etc/fstab.qcom

# restore the original qcom post-boot
mv $BACKUP_LOCATION/init.qcom.post_boot.sh /system/vendor/bin/init.qcom.post_boot.sh

# set permissions on both
chmod 600 $BUILDPROP_FILE; #build.prop

chmod 644 $INIT_FILE; #init.qcom.rc

# fstab
chmod 644 /system/vendor/etc/fstab.qcom

# qcom post-boot
chmod 755 /system/vendor/bin/init.qcom.post_boot.sh

rm -rf $BACKUP_LOCATION;

