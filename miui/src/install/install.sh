#!/sbin/sh

# build prop
BUILDPROP_FILE=/system/build.prop;

# backup location
BACKUP_LOCATION=/data/local/kevp75/BACKUP;

# tweak files location
TWEAK_FILES=/data/local/kevp75;

# make the backup directory if it does not exist
mkdir -p $BACKUP_LOCATION;

# make our tweak file location if it does not exist
mkdir -p $TWEAK_FILES;

# --------------------------------------------------------------------------------------------
# BEGIN build.prop tweaks
# --------------------------------------------------------------------------------------------

# make sure we can write to the build.prop
chmod 777 $BUILDPROP_FILE;

# restore the original backup file if it exists
if [ -f "$BACKUP_LOCATION/build.prop" ]; then
    mv $BACKUP_LOCATION/build.prop $BUILDPROP_FILE;
fi;

# backup the existing build.prop
cp $BUILDPROP_FILE $BACKUP_LOCATION/build.prop;

# inject our build.prop tweaks
# Dalvik VM Tweaks
DVMCHECK=$(grep -i "KEVP75-DALVIK" $BUILDPROP_FILE); 
if [[ ! -n $DVMCHECK ]]; then
	# Remove existing dalvik config lines
	sed -i '/dalvik.vm.heapstartsize/d' $BUILDPROP_FILE
	sed -i '/dalvik.vm.heapgrowthlimit/d' $BUILDPROP_FILE
	sed -i '/dalvik.vm.heapsize/d' $BUILDPROP_FILE
	sed -i '/dalvik.vm.heaptargetutilization/d' $BUILDPROP_FILE
	sed -i '/dalvik.vm.heapminfree/d' $BUILDPROP_FILE
	sed -i '/dalvik.vm.heapmaxfree/d' $BUILDPROP_FILE
	# Now add in my values
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-DALVIK: Reconfigure the dalvik cache" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heapstartsize=8m" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heapgrowthlimit=256m" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heapsize=512m" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heaptargetutilization=0.50" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heapminfree=4m" >> $BUILDPROP_FILE;
	 echo "dalvik.vm.heapmaxfree=16m" >> $BUILDPROP_FILE;
fi;

# FHA Memory Tweaks
FHACHECK=$(grep -i "KEVP75-FHA" $BUILDPROP_FILE); 
if [[ ! -n $FHACHECK ]]; then
	# Remove Existing dha lines
	sed -i '/ro.config.dha_cached_min/d' $BUILDPROP_FILE;
	sed -i '/ro.config.dha_cached_max/d' $BUILDPROP_FILE;
	sed -i '/ro.config.dha_empty_min/d' $BUILDPROP_FILE;
	sed -i '/ro.config.dha_empty_max/d' $BUILDPROP_FILE;
	sed -i '/ro.config.dha_th_rate/d' $BUILDPROP_FILE;
	sed -i '/ro.config.dha_pwhitelist_enable/d' $BUILDPROP_FILE;
	# Remove any existing FHA lines
	sed -i '/ro.config.fha_enable/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.use_trim_settings/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.empty_app_percent/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.trim_empty_percent/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.trim_cache_percent/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.bservice_enable/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.bservice_limit/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.bservice_age/d' $BUILDPROP_FILE;
	sed -i '/ro.sys.fw.trim_enable_memory/d' $BUILDPROP_FILE;
	# Now copy in our fha properties
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-FHA: Enable FHA and configure for our device" >> $BUILDPROP_FILE;
	 echo "ro.config.fha_enable=true" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.use_trim_settings=true" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.empty_app_percent=50" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.trim_empty_percent=100" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.trim_cache_percent=100" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.bservice_enable=true" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.bservice_limit=5" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.bservice_age=5000" >> $BUILDPROP_FILE;
	 echo "ro.sys.fw.trim_enable_memory=2147483648" >> $BUILDPROP_FILE; 
fi;

# HWUI Tweaks
HWUICHECK=$(grep -i "KEVP75-HWUI" $BUILDPROP_FILE);
if [[ ! -n $HWUICHECK ]]; then
	# Remove existing hwui config lines
	sed -i '/ro.hwui.texture_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.layer_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.path_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.texture_cache_flushrate/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.shape_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.gradient_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.drop_shadow_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.r_buffer_cache_size/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.text_small_cache_width/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.text_small_cache_height/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.text_large_cache_width/d' $BUILDPROP_FILE
	sed -i '/ro.hwui.text_large_cache_height/d' $BUILDPROP_FILE
	# Add in our hw ui tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-HWUI: Reconfigure the hwui cache" >> $BUILDPROP_FILE;
	 echo "ro.hwui.texture_cache_size=72" >> $BUILDPROP_FILE;
	 echo "ro.hwui.layer_cache_size=48" >> $BUILDPROP_FILE;
	 echo "ro.hwui.path_cache_size=32" >> $BUILDPROP_FILE;
	 echo "ro.hwui.texture_cache_flushrate=0.4" >> $BUILDPROP_FILE;
	 echo "ro.hwui.shape_cache_size=4" >> $BUILDPROP_FILE;
	 echo "ro.hwui.gradient_cache_size=1" >> $BUILDPROP_FILE;
	 echo "ro.hwui.drop_shadow_cache_size=6" >> $BUILDPROP_FILE;
	 echo "ro.hwui.r_buffer_cache_size=8" >> $BUILDPROP_FILE;
	 echo "ro.hwui.text_small_cache_width=1024" >> $BUILDPROP_FILE;
	 echo "ro.hwui.text_small_cache_height=1024" >> $BUILDPROP_FILE;
	 echo "ro.hwui.text_large_cache_width=4096" >> $BUILDPROP_FILE;
	 echo "ro.hwui.text_large_cache_height=4096" >> $BUILDPROP_FILE;
fi;

# Fling Tweaks
FLINGCHECK=$(grep -i "KEVP75-FLING" $BUILDPROP_FILE);
if [[ ! -n $FLINGCHECK ]]; then
	# Remove existing fling config lines
	sed -i '/windowsmgr.max_events_per_sec/d' $BUILDPROP_FILE;
	sed -i '/ro.min_pointer_dur/d' $BUILDPROP_FILE;
	# Add in our fling tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-FLING" >> $BUILDPROP_FILE;
	 echo "windowsmgr.max_events_per_sec=300" >> $BUILDPROP_FILE;
	 echo "ro.min_pointer_dur=8" >> $BUILDPROP_FILE;
fi;

# Gaming Tweaks
GAMECHECK=$(grep -i "KEVP75-GAMING" $BUILDPROP_FILE);
if [[ ! -n $GAMECHECK ]]; then
	# Remove existing tweaks
	sed -i '/persist.sys.NV_FPSLIMIT/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.NV_POWERMODE/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.NV_PROFVER/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.NV_STEREOCTRL/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.NV_STEREOSEPCHG/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.NV_STEREOSEP/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.purgeable_assets/d' $BUILDPROP_FILE;
	# Add in our gaming tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-GAMING" >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_FPSLIMIT=60 " >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_POWERMODE=1 " >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_PROFVER=15 " >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_STEREOCTRL=0 " >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_STEREOSEPCHG=0 " >> $BUILDPROP_FILE;
	 echo "persist.sys.NV_STEREOSEP=20 " >> $BUILDPROP_FILE;
	 echo "persist.sys.purgeable_assets=1" >> $BUILDPROP_FILE;
fi;

# Performace Tweaks
PERFCHECK=$(grep -i "KEVP75-PERF" $BUILDPROP_FILE);
if [[ ! -n $PERFCHECK ]]; then
	# Remove existing tweaks
	sed -i '/debug.kill_allocating_task/d' $BUILDPROP_FILE;
	sed -i '/force_hw_ui/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.use_dithering/d' $BUILDPROP_FILE;
	sed -i '/ro.min_pointer_dur/d' $BUILDPROP_FILE;
	sed -i '/persist.sys.ui.hw/d' $BUILDPROP_FILE;
	sed -i '/ro.kernel.android.checkjni/d' $BUILDPROP_FILE;
	# Add in our performance tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-PERF" >> $BUILDPROP_FILE;
	 echo "debug.kill_allocating_task=0" >> $BUILDPROP_FILE;
	 echo "force_hw_ui=true" >> $BUILDPROP_FILE;
	 echo "persist.sys.use_dithering=1" >> $BUILDPROP_FILE;
	 echo "ro.min_pointer_dur=1" >> $BUILDPROP_FILE;
	 echo "persist.sys.ui.hw=1" >> $BUILDPROP_FILE;
	 echo "ro.kernel.android.checkjni=0" >> $BUILDPROP_FILE;
fi;

# Power Tweaks
POWERCHECK=$(grep -i "KEVP75-POWER" $BUILDPROP_FILE);
if [[ ! -n $POWERCHECK ]]; then
	# remove the existing tweaks	
	sed -i '/ro.config.hw_power_saving/d' $BUILDPROP_FILE;
	sed -i '/ro.config.hw_fast_dormancy/d' $BUILDPROP_FILE;
	sed -i '/power.saving.mode/d' $BUILDPROP_FILE;
	sed -i '/pm.sleep_mode/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.disable.power.collapse/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.power_collapse/d' $BUILDPROP_FILE;
	sed -i '/power_supply.wakeup/d' $BUILDPROP_FILE;
	sed -i '/ro.mot.eri.losalert.delay/d' $BUILDPROP_FILE;
	sed -i '/ro.config.hw_fast_dormancy/d' $BUILDPROP_FILE;
	sed -i '/ro.config.hw_power_saving/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.disable.power.collapse/d' $BUILDPROP_FILE;
	sed -i '/debug.performance.tuning/d' $BUILDPROP_FILE;
	sed -i '/debug.composition.type/d' $BUILDPROP_FILE;
	sed -i '/debug.sf.hw/d' $BUILDPROP_FILE;
	sed -i '/profiler.force_disable_err_rpt/d' $BUILDPROP_FILE;
	sed -i '/profiler.force_disable_ulog/d' $BUILDPROP_FILE;
	sed -i '/ro.vold.umsdirtyratio/d' $BUILDPROP_FILE;
	sed -i '/dalvik.vm.checkjni/d' $BUILDPROP_FILE;
	sed -i '/dalvik.vm.execution-mode/d' $BUILDPROP_FILE;
	sed -i '/video.accelerate.hw/d' $BUILDPROP_FILE;
	sed -i '/power_supply.wakeup/d' $BUILDPROP_FILE;
	# Add in our power tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-POWER" >> $BUILDPROP_FILE;
	 echo "ro.config.hw_power_saving=1 " >> $BUILDPROP_FILE;
	 echo "ro.config.hw_fast_dormancy=1" >> $BUILDPROP_FILE;
	 echo "power.saving.mode=1" >> $BUILDPROP_FILE;
	 echo "pm.sleep_mode=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.disable.power.collapse=0" >> $BUILDPROP_FILE;
	 echo "ro.ril.power_collapse=1" >> $BUILDPROP_FILE;
	 echo "power_supply.wakeup=enable" >> $BUILDPROP_FILE;
	 echo "ro.mot.eri.losalert.delay=1000" >> $BUILDPROP_FILE;
	 echo "ro.config.hw_fast_dormancy=1" >> $BUILDPROP_FILE;
	 echo "ro.config.hw_power_saving=1 " >> $BUILDPROP_FILE;
	 echo "ro.ril.disable.power.collapse=0 " >> $BUILDPROP_FILE;
	 echo "debug.performance.tuning=1 " >> $BUILDPROP_FILE;
	 echo "debug.composition.type=hw " >> $BUILDPROP_FILE;
	 echo "debug.sf.hw=1" >> $BUILDPROP_FILE;
	 echo "profiler.force_disable_err_rpt=1 " >> $BUILDPROP_FILE;
	 echo "profiler.force_disable_ulog=1 " >> $BUILDPROP_FILE;
	 echo "ro.vold.umsdirtyratio=20 " >> $BUILDPROP_FILE;
	 echo "dalvik.vm.checkjni=false " >> $BUILDPROP_FILE;
	 echo "dalvik.vm.execution-mode=int:jit " >> $BUILDPROP_FILE;
	 echo "video.accelerate.hw=1" >> $BUILDPROP_FILE;
fi;

# Wifi Scanning Tweak
WIFICHECK=$(grep -i "KEVP75-WIFI" $BUILDPROP_FILE);
if [[ ! -n $WIFICHECK ]]; then
	sed -i '/wifi.supplicant_scan_interval/d' $BUILDPROP_FILE
	# Now add ours in
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-WIFI: Wifi Scanning" >> $BUILDPROP_FILE;
	 echo "wifi.supplicant_scan_interval=300" >> $BUILDPROP_FILE;
fi;

# DNS Tweaks
DNSCHECK=$(grep -i "KEVP75-DNS" $BUILDPROP_FILE);
if [[ ! -n $DNSCHECK ]]; then
	# Remove existing fling config lines
	sed -i '/dns1/d' $BUILDPROP_FILE;
	sed -i '/dns2/d' $BUILDPROP_FILE;
	# Add in our dns tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-DNS" >> $BUILDPROP_FILE;
	 echo "net.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.dns2=1.0.0.1" >> $BUILDPROP_FILE;
	 echo "net.rmnet0.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.rmnet0.dns2=1.0.0.1" >> $BUILDPROP_FILE;
	 echo "net.ppp0.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.ppp0.dns2=1.0.0.1" >> $BUILDPROP_FILE;
	 echo "net.wlan0.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.wlan0.dns2=1.0.0.1" >> $BUILDPROP_FILE;
	 echo "net.eth0.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.eth0.dns2=1.0.0.1" >> $BUILDPROP_FILE;
	 echo "net.gprs.dns1=1.1.1.1" >> $BUILDPROP_FILE;
	 echo "net.gprs.dns2=1.0.0.1" >> $BUILDPROP_FILE;
fi;

# Net Tweaks
NETCHECK=$(grep -i "KEVP75-NET" $BUILDPROP_FILE);
if [[ ! -n $NETCHECK ]]; then
	# Remove the existing tweaks first	
	sed -i '/net.ipv4.ip_no_pmtu_disc/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.route.flush/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_ecn/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_fack/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_mem/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_moderate_rcvbuf/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_no_metrics_save/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_rfc1337/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_rmem/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_sack/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_timestamps/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_window_scaling/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_wmem/d' $BUILDPROP_FILE;
	sed -i '/wifi.supplicant_scan_interval/d' $BUILDPROP_FILE;
	sed -i '/net.ipv4.tcp_window_scaling/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.hsxpa/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.gprsclass/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.hep/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.dtm/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.hsdpa.category/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.a53/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.3g.prefix/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.htcmaskw1.bitmask/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.htcmaskw1/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.hsupa.category/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.hsdpa.category/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.a52/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.set.mtu1472/d' $BUILDPROP_FILE;
	sed -i '/persist.cust.tel.eons/d' $BUILDPROP_FILE;
	sed -i '/ro.config.hw_fast_dormancy/d' $BUILDPROP_FILE;
	sed -i '/persist.data_netmgrd_mtu/d' $BUILDPROP_FILE;
	sed -i '/persist.data_netmgrd_nint/d' $BUILDPROP_FILE;
	sed -i '/ro.use_data_netmgrd/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.dtm/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.def.agps.mode/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.def.agps.feature/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.gea3/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.enable.fd.plmn.prefix/d' $BUILDPROP_FILE;
	sed -i '/ro.ril.set.mtu1472/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.default/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.wifi/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.umts/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.gprs/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.edge/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.hspa/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.lte/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.hsdpa/d' $BUILDPROP_FILE;
	sed -i '/net.tcp.buffersize.evdo_b/d' $BUILDPROP_FILE;
	sed -i '/persist.telephony.support.ipv6/d' $BUILDPROP_FILE;
	sed -i '/persist.telephony.support.ipv4/d' $BUILDPROP_FILE;
	# Add in our net tweaks
	 echo " " >> $BUILDPROP_FILE;
	 echo "# KEVP75-NET" >> $BUILDPROP_FILE;
	 echo "net.ipv4.ip_no_pmtu_disc=0" >> $BUILDPROP_FILE;
	 echo "net.ipv4.route.flush=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_ecn=0" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_fack=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_mem=187000 187000 187000" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_moderate_rcvbuf=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_no_metrics_save=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_rfc1337=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_rmem=4096 39000 187000" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_sack=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_timestamps=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_window_scaling=1" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_wmem=4096 39000 18700" >> $BUILDPROP_FILE;
	 echo "wifi.supplicant_scan_interval=220" >> $BUILDPROP_FILE;
	 echo "net.ipv4.tcp_window_scaling=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.hsxpa=3" >> $BUILDPROP_FILE;
	 echo "ro.ril.gprsclass=10" >> $BUILDPROP_FILE;
	 echo "ro.ril.hep=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.dtm=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.hsdpa.category=12" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.a53=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.3g.prefix=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.htcmaskw1.bitmask=4294967295" >> $BUILDPROP_FILE;
	 echo "ro.ril.htcmaskw1=14449" >> $BUILDPROP_FILE;
	 echo "ro.ril.hsupa.category=7" >> $BUILDPROP_FILE;
	 echo "ro.ril.hsdpa.category=10" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.a52=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.set.mtu1472=1" >> $BUILDPROP_FILE;
	 echo "persist.cust.tel.eons=1" >> $BUILDPROP_FILE;
	 echo "ro.config.hw_fast_dormancy=1" >> $BUILDPROP_FILE;
	 echo "persist.data_netmgrd_mtu=1482" >> $BUILDPROP_FILE;
	 echo "persist.data_netmgrd_nint=8" >> $BUILDPROP_FILE;
	 echo "ro.use_data_netmgrd=true" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.dtm=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.def.agps.mode=2" >> $BUILDPROP_FILE;
	 echo "ro.ril.def.agps.feature=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.gea3=1" >> $BUILDPROP_FILE;
	 echo "ro.ril.enable.fd.plmn.prefix=23402,23410,23411" >> $BUILDPROP_FILE;
	 echo "ro.ril.set.mtu1472=1" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.default=4096,87380,256960,4096, 16384,256960" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.wifi=4096,87380,256960,4096,163 84,256960" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.umts=4096,87380,256960,4096,163 84,256960" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.gprs=4096,87380,256960,4096,163 84,256960" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.edge=4096,87380,256960,4096,163 84,256960" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.hspa=6144,87380,524288,6144,163 84,262144" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.lte=524288,1048576,2097152,5242 88,1048576,2097152" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.hsdpa=6144,87380,1048576,6144,8 7380,1048576" >> $BUILDPROP_FILE;
	 echo "net.tcp.buffersize.evdo_b=6144,87380,1048576,6144, 87380,1048576" >> $BUILDPROP_FILE;
	 echo "persist.telephony.support.ipv6=1" >> $BUILDPROP_FILE;
	 echo "persist.telephony.support.ipv4=1" >> $BUILDPROP_FILE;
fi;

# reset the build.prop permissions
chmod 600 $BUILDPROP_FILE;

# --------------------------------------------------------------------------------------------
# END build.prop tweaks
# --------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------
# BEGIN init.rc injector
# --------------------------------------------------------------------------------------------

# the init.rc we need to inject ours into
INITRC=/system/vendor/etc/init/hw/init.qcom.rc

# restore the original backup file if it exists
if [ -f "$BACKUP_LOCATION/init.qcom.rc" ]; then
    mv $BACKUP_LOCATION/init.qcom.rc $INITRC;
fi;

# backup the original
cp $INITRC $BACKUP_LOCATION/init.qcom.rc

# add in our own init.rc
if ! grep -q 'kevp75' $INITRC; then
   sed -i '1i import /system/etc/init/init.kevp75.rc' $INITRC
fi;

# remove the zram swappinig
sed -i '/swap/d' $INITRC
sed -i '/zram0/d' $INITRC

cp /system/vendor/bin/init.qcom.post_boot.sh $BACKUP_LOCATION/init.qcom.post_boot.sh
sed -i '/zram0/d' /system/vendor/bin/init.qcom.post_boot.sh

cp /system/vendor/etc/fstab.qcom $BACKUP_LOCATION/fstab.qcom
sed -i '/swap/d' /system/vendor/etc/fstab.qcom

# now copy in our own init.rc
cp /tmp/init.kevp75.rc /system/etc/init/init.kevp75.rc

# set it's permissions
chmod 644 /system/etc/init/init.kevp75.rc

# --------------------------------------------------------------------------------------------
# END init.rc injector
# --------------------------------------------------------------------------------------------

# make the log dir for our scripts
mkdir -p /data/local/kevp75/logs

# create the user backed exempt lists
mkdir -p /data/local/kevp75/USER
touch /data/local/kevp75/USER/rib_allowed
touch /data/local/kevp75/USER/wl_allowed
