#!/system/bin/sh

LOG=/data/local/kevp75/logs/tweaks.log

# check if we're already running.  if we are, exit
if [ -f /data/local/kevp75/kev_tweaks.running ]; then
	exit 1;
fi;

# If we havent exitted, write a .running file out to make sure we aren't running multiple times
echo 1 > /data/local/kevp75/kev_tweaks.running

echo "--------------------------------------------------------" >> $LOG
echo "--------------------------------------------------------" >> $LOG
echo "KEVS Tweaks Boot" >> $LOG
echo `date` >> $LOG
echo "--------------------------------------------------------" >> $LOG

# update local DNS servers to new cloudflare
su -c "touch /system/etc/resolv.conf"
su -c "chmod 777 /system/etc/resolv.conf"
echo "nameserver 1.1.1.1" > /system/etc/resolv.conf
echo "nameserver 1.0.0.1" >> /system/etc/resolv.conf
su -c "chmod 644 /system/etc/resolv.conf"
iptables -t nat -A OUTPUT -p tcp --dport 53 -j DNAT --to-destination 1.1.1.1:53
iptables -t nat -A OUTPUT -p udp --dport 53 -j DNAT --to-destination 1.0.0.1:53
iptables -t nat -I OUTPUT -p tcp --dport 53 -j DNAT --to-destination 1.1.1.1:53
iptables -t nat -I OUTPUT -p udp --dport 53 -j DNAT --to-destination 1.0.0.1:53
setprop net.eth0.dns1 1.1.1.1
setprop net.eth0.dns2 1.0.0.1
setprop net.dns1 1.1.1.1
setprop net.dns2 1.0.0.1
setprop net.ppp0.dns1 1.1.1.1
setprop net.ppp0.dns2 1.0.0.1
setprop net.rmnet0.dns1 1.1.1.1
setprop net.rmnet0.dns2 1.0.0.1
setprop net.rmnet1.dns1 1.1.1.1
setprop net.rmnet1.dns2 1.0.0.1
setprop net.pdpbr1.dns1 1.1.1.1
setprop net.pdpbr1.dns2 1.0.0.1

# Some cleaning
su -c "rm -f /cache/*.apk;"
su -c "rm -f /cache/*.tmp;"
su -c "rm -f /cache/recovery/*;"
su -c "rm -f /data/*.log;"
su -c "rm -f /data/*.txt;"
su -c "rm -f /data/log/*.*;"
su -c "rm -f /data/local/*.log;"
su -c "rm -f /data/local/tmp/*.*;"
su -c "rm -f /data/last_alog/*;"
su -c "rm -f /data/last_kmsg/*;"
su -c "rm -f /data/mlog/*;"
su -c "rm -rf /data/tombstones/*;"
su -c "rm -f /data/system/dropbox/*;"
su -c "rm -f /data/system/usagestats/*.*;"
su -c "chmod 700 /data/system/usagestats;"
su -c "rm -rf /storage/emulated/0/LOST.DIR;"

# Turn off logging
stop logd

# Disable Media Scanning
pm disable com.android.providers.media/com.android.providers.media.MediaScannerReceiver;

# Kill debugging
su -c "sysctl -w vm.panic_on_oom=0"
su -c "sysctl -w kernel.panic_on_oops=0"
su -c "sysctl -w kernel.panic=0"

# Perfect Mounts
su -c "mount -o remount,noatime,noauto_da_alloc,nodiratime,barrier=0,nobh /system;"
su -c "mount -o remount,noatime,nodiratime /data;"
su -c "mount -o remount,noatime,noauto_da_alloc,nosuid,nodev,nodiratime,barrier=0,nobh /vendor;"
su -c "mount -o remount,noatime,nodiratime,rw,errors=remount-ro /storage/emulated;"
su -c "mount -o remount,nosuid,nodev,noatime -t auto /proc;"
su -c "mount -o remount,nosuid,nodev,noatime,barrier=0,data=writeback,nobh,journal_async_commit,noauto_da_alloc,commit=60,discard -t auto /sys;"

# Block level scheduler queue tweaks
for i in /sys/block/*/queue; do
	su -c "echo 0 > $i/add_random;" # stock varies
	su -c "echo 0 > $i/iostats;" # stock is 1
	su -c "echo 64 > $i/nr_requests;" # stock is 128
	su -c "echo 0 > $i/rotational;" # stock varies
	su -c "echo 1 > $i/rq_affinity;" # stock is 0 or 1
	su -c "echo "noop" > $i/scheduler;" # stock is cfq
	su -c "echo "deadline" > $i/scheduler;" # stock is cfq
	su -c "echo 250 > $i/iosched/read_expire;"
	su -c "echo 2500 > $i/iosched/write_expire;"
	su -c "echo 1 > $i/iosched/writes_starved;"
	su -c "echo 1 > $i/iosched/front_merges;"
	su -c "echo 8 > $i/iosched/fifo_batch;"
done;

# turn off swapping... just in case its still here at this point
su -c "swapoff /dev/block/zram0"

# Transmission queue buffer tweak
for i in $(find /sys/class/net -type l); do
	su -c "echo 512 > $i/tx_queue_len;"
done;

# battery calibration
(
while true
do

	# check the level of the battery
	LEVEL=$(cat /sys/class/power_supply/battery/capacity);
	
	# check the draw
	CUR=$(cat /sys/class/power_supply/battery/current_now);
	
	# if the battery is fully charged, and there is no draw
	if [ "$LEVEL" == "100" ] && [ "$CUR" == "0" ] ; then
	
		# remove the bin file
		rm -f /data/system/batterystats.bin;
	fi;

	# sleep for 1 week
	sleep 7d
done
) & 

# Trim every week, and on boot
(
while true
do

	echo "--------------------------------------------------------" >> $LOG
	echo "Trim Run: " >> $LOG
	echo `date` >> $LOG
	echo "--------------------------------------------------------" >> $LOG

	su -c "mount -o remount,rw /system;"
	su -c "mount -o remount,rw /data;"
	su -c "mount -o remount,rw /vendor;"
	su -c "fstrim -v /system;"
	su -c "fstrim -v /data;"
	su -c "fstrim -v /vendor;"
	su -c "mount -o remount,ro /system;"
	su -c "mount -o remount,ro /vendor;"

	# sleep for 1 week
	sleep 7d
done
) &

# remove the .running file
rm -f /data/local/kevp75/kev_tweaks.running
